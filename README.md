# Fastwork Interview Send Mail API

Fastwork Interview API using Docker, Node.js, Express

---

## 1.First Step

install [Docker](https://docs.docker.com/install/#supported-platforms)

## 2.Running Email Api And Database

เพื่อรัน **email service** และ **mognodb** 2 ตัวสำหรับใช้งานจริงและสำหรับ test environment

```sh
$ npm install
$ docker-compose up
```

apiจะรันอยู่ที่ [localhost:8000](http://localhost:8000/)

## 3.Running Test

รันเทสข้างนอก container โดยจะต้องรัน step2 ก่อนเพื่อใช้งาน mongo สำหรับ test

```sh
$ open new terminal and cd <to project>
$ npm run test:unit หรือ npm run test:integration
```

---

# อธิบายเพิ่มเติม

---

## File Structor

วาง structor code แบบ module base (โดย route, controller, model, schema จะอยู่ใน folder เดียวกันของ module นั้นๆ เพื่อให้สามารถ dev feature นั้นได้ง่ายๆโดยไม่ต้องเปิดหลาย folder)

## เทคโนลียีที่ใช้

1. node.js + express
2. docker + docker-compose: เลือกใช้เพราะทำให้ไม่มีปัญหาเรื่องการ deploy แล้ว environment ไม่ตรง และที่สำคัญสามารถลง mongo db หลายตัว เพื่อสำหรับใช้งานจริงและใช้เพื่อเทสได้ง่ายมากๆ
3. mongo + mongoose
4. mocha + chai + sinon + supertest สำหรับ test

## email provider

1. mail gun: เลือกใช้เพราะเป็น provider ที่มี dev community และ example เยอะ ทำให้ dev ได้ง่าย
   **ปัญหาที่เจอ:** mailhost ของ mailgun ที่ให้มานั้นเป็น hostmail สำหรับเทสเท่านั้นทำให้สามารถส่งเมลล์ไปยังปลายทางได้แค่ 1 เมลล์โดยเพิ่มสูงสุดได้เป็น 5 เมลล์เท่านั้น
   **วิธีแก้:** สร้าง domain ใหม่ขึ้นมาแล้วเพิ่ม config ให้ใช้ provider เป็น mailgun
2. gmail: เลือกใช้เพราะง่ายต่อการพัฒนามากๆ และ gmail ก็เป็น host ที่มีความน่าเชื่อถือสูงและไม่ต้องทำการผูกกับ domain ของตัวเองทำให้สามารถนำมาใช้ในกรณีที่ provider หลักมีปัญหาได้

\*\*เลือกให้ใช้ mailgun เป็น provider หลักในการส่งเมลล์เพราะเป็น provider ที่มีการเก็บสถิติต่างๆและสามารถสร้าง api เพื่อขอข้อมูลจาก provider ได้ แต่หากตัว mail gun มีปัญหาไม่ว่าจากตัว mail gun เองหรือจาก domain ที่ผูกไว้ตัวระบบจะทำการใช้ gmail ให้อัตโนมัติ

## deploy

digital ocean: เลือกใช้เพราะราคาจับต้องได้, เป็น ssd,มี community พอสมควร และสามารถใช้งานได้ง่าย

## อื่นๆ

\*\* สิ่งที่ควรทำเพิ่มเติมเพื่อเพิ่มความปลอดภัยให้กับ api ได้อีกคือ ปรับให้เป็น https,http/2 และทำการ authen mongodb หรือใช้ mongodb จาก provider แทน
