import { mailRoute } from './modules'

export default app => {
  app.use('/api/v1/mail', mailRoute)

  app.use((req, res) => {
    res.status(404).json({
      error: 404,
      description: 'Not found',
      url: req.originalUrl
    })
  })
}
