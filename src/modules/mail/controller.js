import { mailGunLib } from '../../libs/mailGun'
import { gmail } from '../../libs/gmail'
import mailModel from './model'

export default {
  async send(req, res) {
    const { body: params } = req

    try {
      await mailGunLib.sendMail(params)
      const recordSuccessMailgun = await mailModel.create(params, {
        name: 'mailGun',
        isSuccess: true
      })

      return res.status(200).json(recordSuccessMailgun)
    } catch (errMailGun) {
      const recordFailureMailGun = await mailModel.create(params, {
        name: 'mailGun',
        isSuccess: false
      })

      try {
        await gmail.sendMail(params)
        const updateRecordResponse = await mailModel.addProviderById(
          recordFailureMailGun,
          {
            name: 'gmail',
            isSuccess: true
          }
        )

        return res.status(200).json(updateRecordResponse)
      } catch (errGmail) {
        await mailModel.addProviderById(recordFailureMailGun, {
          name: 'gmail',
          isSuccess: false
        })

        return res.status(400).json({
          messageError: {
            mailgun: errMailGun,
            gmail: errGmail
          }
        })
      }
    }
  },

  async history(req, res) {
    try {
      const mailHistory = await mailModel.all()
      return res.status(200).json(mailHistory)
    } catch (err) {
      return res.status(400).json(err)
    }
  }
}
