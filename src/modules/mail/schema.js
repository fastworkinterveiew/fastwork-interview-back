import mongoose from 'mongoose'

const Schema = mongoose.Schema
const mail = new Schema(
  {
    from: { type: String, required: true },
    to: { type: String, required: true },
    subject: { type: String, required: true },
    html: { type: String, required: true },
    providers: [
      {
        name: { type: String, required: true },
        success: { type: Boolean, required: false }
      }
    ],

    publishDate: { type: Date, default: Date.now }
  },
  {
    versionKey: false
  }
)

const mailModel = mongoose.model('mail', mail)

export default mailModel
