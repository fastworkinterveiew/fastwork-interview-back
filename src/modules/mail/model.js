import mailSchema from './schema'

export default {
  create: (params, provider) => {
    const data = {
      ...params,
      providers: [
        {
          name: provider.name,
          success: provider.isSuccess
        }
      ]
    }
    return mailSchema.create(data)
  },
  addProviderById: (oldData, provider) => {
    const query = {
      _id: oldData._id
    }
    const newProvider = {
      name: provider.name,
      success: provider.isSuccess
    }

    return mailSchema.findOneAndUpdate(
      query,
      { $push: { providers: newProvider } },
      { upsert: false, new: true }
    )
  },

  all: () => mailSchema.find()
}
