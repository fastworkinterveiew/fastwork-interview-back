import validate from 'express-validation'
import Joi from 'joi'

const sendMail = validate({
  body: {
    from: Joi.string().required(),
    to: Joi.string().required(),
    subject: Joi.string().required(),
    html: Joi.string().required()
  }
})

export const validator = {
  sendMail
}
