import express from 'express'
import mailController from './controller'
import { validator } from './validator'

const router = express.Router()

router.post('/send', validator.sendMail, mailController.send)
router.get('/history', mailController.history)

export default router
