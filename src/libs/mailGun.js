import mailgun from 'mailgun-js'
import mailComposer from 'nodemailer/lib/mail-composer'

import { MAIL_GUN } from '../config'

const mailgunService = mailgun({
  apiKey: MAIL_GUN.API_KEY,
  domain: MAIL_GUN.DOMAIN
})

const mailGunLib = {
  sendMail(params) {
    const mail = new mailComposer(params)

    return new Promise((resolve, reject) => {
      mail.compile().build((err, message) => {
        const dataToSend = {
          to: params.to,
          message: message.toString('ascii')
        }
        mailgunService.messages().sendMime(dataToSend, (err, body) => {
          if (err) {
            reject(err)
          }
          resolve(body)
        })
      })
    })
  }
}
export { mailGunLib }
