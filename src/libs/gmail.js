import nodemailer from 'nodemailer'
import { GMAIL } from '../config'

const gmail = {
  sendMail(params) {
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: GMAIL.USER,
        pass: GMAIL.PASS
      }
    })

    const mailOptions = {
      ...params,
      from: `${params.from} <worapon@udrinkidrive.co.th>`
    }

    return transporter.sendMail(mailOptions)
  }
}
export { gmail }
