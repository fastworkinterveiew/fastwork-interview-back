import dotenv from 'dotenv'
dotenv.config()

const GMAIL = {
  USER: process.env.GMAIL_USER,
  PASS: process.env.GMAIL_PASS
}

export default GMAIL
