import dotenv from 'dotenv'
dotenv.config()

const DATABASE = {
  HOST: process.env.DATABASE_HOST,
  DATABASE_NAME: process.env.DATABASE_NAME
}

if (process.env.NODE_ENV === 'test') {
  DATABASE.HOST = 'mongodb://localhost:27018'
}

export default DATABASE
