import dotenv from 'dotenv'
dotenv.config()

const MAIL_GUN = {
  API_KEY: process.env.MAILGUN_API_KEY,
  DOMAIN: process.env.MAILGUN_DOMAIN
}

if (process.env.NODE_ENV === 'test') {
  MAIL_GUN.API_KEY = 'a84017374d9832a58d33b54a3070e14b-3939b93a-8707c8c4'
  MAIL_GUN.DOMAIN = 'sandboxc012fafbaf1f446487dfceb3a51b1fe7.mailgun.org'
}

export default MAIL_GUN
