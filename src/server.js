import 'babel-polyfill'
import express from 'express'
import dotenv from 'dotenv'
import bodyParser from 'body-parser'
import compression from 'compression'
import cors from 'cors'
import mongoose from 'mongoose'

import { DATABASE } from './config'
import routes from './routes'

dotenv.config()

const app = express()
const port = process.env.NODE_PORT || 8000
const database = `${DATABASE.HOST}/${DATABASE.DATABASE_NAME}`

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(compression())

mongoose.connect(
  database,
  { useNewUrlParser: true }
)
mongoose.set('useFindAndModify', false)

routes(app)

if (process.env.NODE_ENV !== 'test') {
  app.listen(port, () => console.info(`app listening on port ${port}`))
}

export default app
