FROM node:8.11.3-alpine
LABEL description="fastwork interview"
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN npm install
EXPOSE 8000
CMD npm run build && npm run start

