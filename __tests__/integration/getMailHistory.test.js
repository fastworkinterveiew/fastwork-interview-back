import sinon from 'sinon'
import { expect, use } from 'chai'
import chaiExclude from 'chai-exclude'
import mongoose from 'mongoose'
import request from 'supertest'

import mailSchema from '../../src/modules/mail/schema'
import app from '../../src/server'
import { mailGunLib } from '../../src/libs/mailGun'
import { gmail } from '../../src/libs/gmail'

use(chaiExclude)

describe('[INTEGRATION][API]', function() {
  this.timeout(8000)
  const sandbox = sinon.createSandbox()
  const input = {
    from: 'worapontest@outlook.com',
    to: 'woraponok@gmail.com',
    subject: 'Test email subject',
    html: '<b> Test email text </b>'
  }

  afterEach(function(done) {
    sandbox.restore()
    mongoose.connection.collections['mails'].drop(function(err) {
      done()
    })
  })

  after(function(done) {
    mongoose.models = {}
    mongoose.modelSchemas = {}
    mongoose.connection.close()
    done()
  })

  describe('[get history]', function() {
    it(`should be return mail history when db has record`, async function() {
      const input = {
        html: 'test',
        from: 'woraponok@outlook.com',
        to: 'woraponok@gmail.com',
        subject: 'testSubject',
        providers: [
          {
            name: 'mailGun',
            success: false
          },
          {
            name: 'gmail',
            success: true
          }
        ]
      }
      await mailSchema.create(input)

      return request(app)
        .get('/api/v1/mail/history')
        .set('Accept', 'application/json')
        .expect(200)
        .then(res => {
          return expect(res.body[0])
            .excludingEvery(['_id', 'publishDate'])
            .to.deep.include(input)
        })
    })
  })

  describe('[send mail]', function() {
    it(`should be return send mail response and send mail success when mailGun is resolve`, async function() {
      return request(app)
        .post('/api/v1/mail/send')
        .set('Accept', 'application/json')
        .send(input)
        .expect(200)
        .then(res => {
          const expected = {
            ...input,
            providers: [{ name: 'mailGun', success: true }]
          }
          return expect(res.body)
            .excludingEvery(['_id', 'publishDate'])
            .to.deep.include(expected)
        })
    })

    it(`should be return send mail response and send mail success when mailGun is reject but gmail is resolve`, async function() {
      sandbox.stub(mailGunLib, 'sendMail').rejects('fail')

      return request(app)
        .post('/api/v1/mail/send')
        .set('Accept', 'application/json')
        .send(input)
        .expect(200)
        .then(res => {
          const expected = {
            ...input,
            providers: [
              { name: 'mailGun', success: false },
              { name: 'gmail', success: true }
            ]
          }
          return expect(res.body)
            .excludingEvery(['_id', 'publishDate'])
            .to.deep.include(expected)
        })
    })

    it(`should be return fail response  when mailGun and gmail is reject`, async function() {
      sandbox.stub(mailGunLib, 'sendMail').rejects('fail')
      sandbox.stub(gmail, 'sendMail').rejects('fail')

      return request(app)
        .post('/api/v1/mail/send')
        .set('Accept', 'application/json')
        .send(input)
        .expect(400)
        .then(res => {
          return expect(res.body).to.have.a.property('messageError')
        })
    })
  })
})
