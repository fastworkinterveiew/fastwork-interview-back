import sinon from 'sinon'
import { expect, use } from 'chai'
import mongoose from 'mongoose'
import chaiAsPromised from 'chai-as-promised'

import { DATABASE } from '../../../../src/config'
import mailController from '../../../../src/modules/mail/controller'
import mailModel from '../../../../src/modules/mail/model'
import { mailGunLib } from '../../../../src/libs/mailGun'
import { gmail } from '../../../../src/libs/gmail'

use(chaiAsPromised)

describe('[UNIT] mail controller', function() {
  this.timeout(5000)
  const sandbox = sinon.createSandbox()
  const dataMailHistoryResolve = [
    {
      from: 'worapontest@outlook.com',
      to: 'woraponok@gmail.com',
      subject: 'Test email subject',
      html: '<b> Test email text </b>',
      providers: [
        {
          name: 'mailGun',
          success: true
        }
      ]
    }
  ]

  before(function() {
    const database = `${DATABASE.HOST}/${DATABASE.DATABASE_NAME}`
    mongoose.connect(
      database,
      { useNewUrlParser: true }
    )
    mongoose.set('useFindAndModify', false)
  })

  afterEach(function() {
    sandbox.restore()
  })

  after(function(done) {
    mongoose.models = {}
    mongoose.modelSchemas = {}
    mongoose.connection.close()
    done()
  })

  describe('[get history]', function() {
    it(`should be pass when mailModel.all is resolve data`, async function() {
      sandbox.stub(mailModel, 'all').resolves(Promise.resolve(dataMailHistoryResolve))

      const res = {
        json(responseJson) {
          const expected = dataMailHistoryResolve
          const actual = responseJson
          expect(actual).to.deep.equal(expected)
          return this
        },
        status(responseStatus) {
          const expected = 200
          const actual = responseStatus
          expect(actual).to.equal(expected)
          return this
        }
      }

      await mailController.history({}, res)
    })

    it(`should be fail when mailModel.all is reject data`, async function() {
      const mailHistoryReject = 'Error while performing Query.'
      sandbox.stub(mailModel, 'all').rejects(mailHistoryReject)

      const res = {
        json(responseJson) {
          expect(responseJson.name).to.equal(mailHistoryReject)
          return this
        },
        status(responseStatus) {
          expect(responseStatus).to.equal(400)
          return this
        }
      }

      await mailController.history({}, res)
    })
  })

  describe('[send mail]', function() {
    it(`should be pass when mailGun and mailModel.create is resolve data`, async function() {
      sandbox.stub(mailGunLib, 'sendMail').resolves(Promise.resolve({}))
      sandbox.stub(mailModel, 'create').resolves(Promise.resolve(dataMailHistoryResolve))

      const res = {
        json(responseJson) {
          const actual = responseJson
          const expected = dataMailHistoryResolve

          expect(actual).to.deep.equal(expected)

          return this
        },
        status(responseStatus) {
          const actual = responseStatus
          const expected = 200

          expect(actual).to.equal(expected)

          return this
        }
      }

      await mailController.send({ body: 'test' }, res)
    })

    it(`should be pass when mailGun or mailModel.create is reject data but gmail is resolve data`, async function() {
      sandbox.stub(mailGunLib, 'sendMail').rejects('fail')
      sandbox.stub(mailModel, 'create').resolves({})
      sandbox.stub(gmail, 'sendMail').resolves({})
      sandbox.stub(mailModel, 'addProviderById').resolves(dataMailHistoryResolve)

      const res = {
        json(responseJson) {
          const actual = responseJson
          const expected = dataMailHistoryResolve

          expect(actual).to.deep.equal(expected)

          return this
        },
        status(responseStatus) {
          const actual = responseStatus
          const expected = 200

          expect(actual).to.equal(expected)

          return this
        }
      }

      await mailController.send({ body: 'test' }, res)
    })

    it(`should be fail when mailGun or mailModel.create is reject data and gmail is reject data`, async function() {
      sandbox.stub(mailGunLib, 'sendMail').rejects('fail')
      sandbox.stub(mailModel, 'create').resolves({})
      sandbox.stub(gmail, 'sendMail').rejects('fail')
      sandbox.stub(mailModel, 'addProviderById').resolves({})

      const res = {
        json(responseJson) {
          const actual = responseJson

          expect(actual).to.have.a.property('messageError')

          return this
        },
        status(responseStatus) {
          const actual = responseStatus
          const expected = 400

          expect(actual).to.equal(expected)

          return this
        }
      }

      await mailController.send({ body: 'test' }, res)
    })
  })
})
